# ch-flash

A program for flashing the CH573 devices.

this is a fork of [](https://github.com/frank-zago/isp55e0.git) with the notable
difference that it will wait for the CH573 device to be connected if one is not
connected already.

This has the benefit of not needing to "race the clock" when flashing the device
because the CH537 (or at least the ones I have) have an unstable usb interface
and will disconnect and reset after several seconds of being connected to the
machine.

With this new utility one can run the flash program and then connect the device
when it will be flashed.

## udev

To flash the device requires some permissions to set up. One can run this flash
program as root to flash the device, but for those who wish to keep the flashing
under-privliged, one can add the following udev rules in
`/etc/udev/rules.d/50-ch573.rules`:

```
SUBSYSTEM=="usb", ATTR{idVendor}=="4348", ATTR{idProduct}=="55e0", MODE:="0666"
```

Then run the command:

`udevadm control --reload-rules`
